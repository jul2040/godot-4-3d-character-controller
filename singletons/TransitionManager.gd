extends Node

var is_first:bool = true
@export var transition_duration:float = 0.5

@onready var shader:ShaderMaterial = $ColorRect.get_material()
@onready var overlay:ColorRect = $ColorRect

func reload_current_scene():
	change_scene("")

func change_scene(to:String):
	change_scene_to(load(to))

var transitioning = false
func change_scene_to(to:PackedScene):
	if(transitioning):
		return
	
	overlay.visible = true
	transitioning = true
	
	shader.set_shader_param("invert", false)
	shader.set_shader_param("progress", 0.0)
	
	var tween:Tween = get_tree().create_tween()
	tween.tween_method(update_progress, 0.0, 1.0, transition_duration)
	await tween.finished
	
	shader.set_shader_param("invert", true)
	shader.set_shader_param("progress", 0.0)
	
	if(to == null):
		get_tree().reload_current_scene()
	else:
		get_tree().change_scene_to(to)
	
	tween.kill()
	tween = get_tree().create_tween()
	tween.tween_method(update_progress, 0.0, 1.0, transition_duration)
	tween.play()
	await tween.finished
	
	transitioning = false
	overlay.visible = false

func update_progress(p:float):
	shader.set_shader_param("progress", p)
