extends Node2D

# set the position of the white dot to the mouse position every frame
func _process(_delta):
	position = get_global_mouse_position()

# display this node as a white dot
func _draw():
	draw_circle(Vector2(), 10, Color.WHITE)
