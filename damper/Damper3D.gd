extends Node3D
class_name Damper3D

# this is for dynamic damping of values
# if you know the start value, end value, and duration in advance, use a tween instead
# this node is for if you know the start value, but not the end value or duration

# inspired by this video: https://www.youtube.com/watch?v=KPoeNZZ6H4s

# multiplies the motion of this node by a factor
# useful for limiting motion to one axis
@export var movement_multiplier := Vector3(1, 1, 1)
# clamps the offset of this node between these values
@export var max_offset := Vector3(INF, INF, INF)
@export var min_offset := Vector3(-INF, -INF, -INF)

# natural resonant frequency (in Hertz)
# keep greater than 0
@export var f:float = 10.0 :
	set(new):
		f = new
		compute_constants()
# damping coefficient
# 0 = no damping
# larger values = more damping
# keep greater than 0
@export var z:float = 0.5 :
	set(new):
		z = new
		compute_constants()
# initial response
# negative values cause "undershoot"
# positive values cause "overshoot"
@export var r:float = 0.0 :
	set(new):
		r = new
		compute_constants()

var k1:float
var k2:float
var k3:float
func compute_constants():
	k1 = z / (PI * f)
	k2 = 1 / (2 * PI * f)
	k3 = r * z / (2 * PI * f)

func _ready():
	compute_constants()

func max3(a:float, b:float, c:float):
	return max(max(a, b), c)

var prev_input = null
var output = null
var output_d = Vector3.ZERO
func update(delta:float, input:Vector3) -> Vector3:
	if(prev_input == null):
		prev_input = input
	if(output == null):
		output = input
	var input_d = (input - prev_input) / delta
	prev_input = input
	var k2_stable:float = max3(k2, delta*delta/2 + delta*k1/2, delta*k1)
	output = output + delta*output_d
	output_d = output_d + delta * (input + k3*input_d - output - k1*output_d) / k2_stable
	return output

func reset():
	output_d = Vector3.ZERO
	prev_input = null
	output = null

@onready var parent = get_parent()
func _process(delta):
	var damped_position = (update(delta, parent.position)-parent.position)*movement_multiplier
	position = damped_position
	position.x = clamp(position.x, min_offset.x, max_offset.x)
	position.y = clamp(position.y, min_offset.y, max_offset.y)
	position.z = clamp(position.z, min_offset.z, max_offset.z)
