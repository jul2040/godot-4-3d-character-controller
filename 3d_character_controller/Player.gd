extends RigidDynamicBody3D

# how fast you accelerate while touching the floor
@export var floor_accel = 4.0
# how fast you accelerate while in the air
@export var air_accel = 0.75
# hard cap on max speed
@export var max_speed = 200.0
# extra floor friction applied by this script
# godot's physics applies some extra friction, which is nessecary to avoid sliding on slopes
@export var floor_friction = 0.1
# initial velocity for jumping
@export var jump_velocity = 30.0

# used to detect when we are on the floor
@export var floor_ray:RayCast3D
@export var camera_arm:SpringArm3D

func _integrate_forces(physics_state:PhysicsDirectBodyState3D):
	# get the direction we want to go in
	var input:Vector2 = get_walk_direction()
	# get the normal vector of the floor
	var floor_normal:Vector3 = Vector3.UP
	var is_on_floor:bool = false
	if(floor_ray.is_colliding()):
		var n:Vector3 = floor_ray.get_collision_normal()
		# it only counts as the floor if its within a certain angle of flat
		if(n.angle_to(Vector3.UP) < PI/4):
			floor_normal = n
			is_on_floor = true
	# some vector math so that we walk on slopes properly
	var direction:Vector3 = Vector3(input.x, 0, input.y)
	var dir_length = direction.length()
	direction = direction.slide(floor_normal)
	direction = direction.normalized()*dir_length
	# choose the correct acceleration
	var accel = floor_accel if is_on_floor else air_accel
	# apply the walk acceleration
	physics_state.linear_velocity += direction*accel
	# calculating velocity along the floor
	var floor_velocity = physics_state.linear_velocity.slide(floor_normal)
	var current_speed:float = floor_velocity.length()
	if(is_on_floor):
		current_speed -= current_speed*floor_friction
	current_speed = clamp(current_speed, 0, max_speed)
	# apply max speed
	physics_state.linear_velocity = floor_velocity.normalized()*current_speed+physics_state.linear_velocity.project(floor_normal)
	if(is_on_floor):
		if(should_jump()):
			physics_state.linear_velocity.y = jump_velocity
	# we need to reset in here because this is a RigidDynamicBody
	if(should_reset):
		should_reset = false
		physics_state.transform.origin = reset_pos
		# wait a couple frames and then reset the camera damper
		await get_tree().process_frame
		await get_tree().process_frame
		$Damper3D.reset()

@onready var reset_pos:Vector3 = position
var should_reset:bool = false
func reset():
	should_reset = true
	linear_velocity = Vector3()

func get_walk_direction() -> Vector2:
	# this ensures that we get a vector with controller deadzones applied properly
	var dir:Vector2 = Input.get_vector("left", "right", "up", "down")
	# walk in the direction we are looking
	dir = dir.rotated(-camera_arm.rotation.y)
	return dir

func should_jump() -> bool:
	return Input.is_action_just_pressed("jump")
