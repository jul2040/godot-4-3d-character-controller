extends Area3D

func _on_kill_area_body_entered(body):
	if(body.is_in_group("player")):
		# when the player falls off the map, reset them to their start position
		body.reset()
